#!/usr/bin/python
# -*- coding: utf-8 -*-

from operator import pos
import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
import emoji, re, string, nltk, logging, os
from plotly import graph_objs as go
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from collections import Counter
from itertools import chain

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn.dummy import DummyClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier

#nltk.download("wordnet")
###################
# CONSTANTS
MIN_WORD_LEN                = 1     # minimal word length
BALANCED_SIZE               = 2000  # size of fake, real dataset each (1500 means 1500 fake and 1500 real => 3000)

# SWITCHES
BALANCED_DATASET            = True
DO_WORD_COUNTS              = True # will count words
DO_NGRAM_COUNTS             = True # will count ngrams
SHOW_GRAPH_DATASET_BALANCE  = True # will plot graph with dataset balance
SHOW_GRAPH_TEXT_LEN         = True # will plot graph with text lengths
DO_PLOT_CLASSIFICATIONS     = True # will plot classifier graph 

###################

#nltk.download("stopwords")
logging.basicConfig(format="%(asctime)s : %(message)s", level=logging.INFO)

stop_words = stopwords.words('english')
stemmer    = PorterStemmer()
lemmatizer = WordNetLemmatizer()

###################
# Functions block
def remove_emoji(text):
    return emoji.get_emoji_regexp().sub(u'', text)

def remove_stopwords(text):
    return " ".join(word for word in text.split(" ") if word not in stop_words)

def stemm_text(text):
    return " ".join(stemmer.stem(word) for word in text.split(" "))

def lemmatize_text(text):
    return " ".join(lemmatizer.lemmatize(word) for word in text.split(" "))

def text_encode(text):
    return text.encode("utf8", errors="replace").decode("utf8")

def clean_text(text):
    '''Make text lowercase, remove text in square brackets,remove links,remove punctuation
    and remove words containing numbers.'''
    text = emoji.get_emoji_regexp().sub(u'', text)
    text = str(text).lower()
    text = re.sub('\[.*?\]', '', text)
    text = re.sub('https?://\S+|www\.\S+', '', text)
    text = re.sub('<.*?>+', '', text)
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    text = re.sub('\n', '', text)
    text = re.sub('\w*\d\w*', '', text)
    return text

###################
# returns a dict with ngrams and their count
def get_ngram_counts(n, sentences):
    if not DO_NGRAM_COUNTS:
        return
    ngrams          = [nltk.ngrams(nltk.word_tokenize(sentence), n) for sentence in sentences]
    ngrams_list	    = list(chain.from_iterable(ngrams))
    logging.info(ngrams_list[:20])
    ngrams_counts   = {k: v for k, v in sorted(Counter(ngrams_list).items(), key=lambda item: item[1], reverse=True)}

    #logging.info(ngrams_counts)
    output = ""
    for k in ngrams_counts:
        output +=  " ".join(k) + "," + str(ngrams_counts[k]) + "\n"
        #output += str(k) + "," + str(ngrams_counts[k]) + "\n"
        #logging.info(str(k) + str(ngrams_counts[k]))
    #os.exit()
    if not os.path.exists("n_grams"):
        os.makedirs("n_grams")
    file_name = "./n_grams/" + str(n) + "_grams" + ("_balanced.txt" if BALANCED_DATASET else ".txt")
    ngrams_file = open(file_name, "w", encoding="utf8")
    ngrams_file.write(output)
    ngrams_file.close()

    return ngrams_counts
###################
# returns (array of all words, dict with every words count)
def get_words_count(sentences, min_word_length = MIN_WORD_LEN):
    words = []
    words_count = {}
    for line in sentences:
        words += [word.lower() for word in nltk.word_tokenize(str(line)) if word.isalnum() and len(word) > min_word_length]
    for x in words:
        key = x.lower()
        words_count[key] = words_count.get(key, 0) + 1
    words_count = sorted(words_count.items(), key=lambda item: item[1], reverse=True)
    return (words, words_count)
###################
def my_plot(classifier, x_test, y_test, title, labels, normalize = None):
    disp = plot_confusion_matrix(classifier, x_test, y_test,display_labels=labels,cmap=plt.cm.Blues, normalize=normalize)
    disp.ax_.set_title(title)
    #print(title)
    #print(disp.confusion_matrix)
###################
# helper function, converts array of word counts into string with "word,count" on every line
def get_words_count_output(arr):
    output = ""
    for word, count in arr:
        output += word.encode("utf8", errors="replace").decode("utf8") + "," + str(count) + "\n"
    return output
# gets word counts for dataframe, then writes them to file
def get_words_counts(df, info, file_name):
    if not DO_WORD_COUNTS:
        return
    logging.info("\n")
    logging.info(info)
    true_df = df[df["target"] == 1]
    fake_df = df[df["target"] == 0]

    sentences     = []
    pos_sentences = []
    neg_sentences = []

    for i, line in df.iterrows():
        sentences.append(df.at[i, "text"])

    for i, line in true_df.iterrows():
        pos_sentences.append((1, true_df.at[i, "text"]))

    for i, line in fake_df.iterrows():
        neg_sentences.append((0, fake_df.at[i, "text"]))

    ###################
    logging.info("counting all words ...")
    # Create the dictionary with word frequencies for the entire set. words = []

    words, words_count           = get_words_count(sentences)
    logging.info("counting words in positive sentences ...")
    words_true, words_true_count = get_words_count(pos_sentences)
    logging.info("counting words in negative sentences ...")
    words_fake, words_fake_count = get_words_count(neg_sentences)

    logging.info("counting done")
    logging.info("top 10 all words")
    logging.info(words_count[:10])
    logging.info("top 10 words positive sentences")
    logging.info(words_true_count[:10])
    logging.info("top 10 words negative sentences")
    logging.info(words_fake_count[:10])
    
    if not os.path.exists("words_count"):
        os.makedirs("words_count")
    

    all = open("./words_count/" + file_name + "_all" + ("_balanced" if BALANCED_DATASET else "") + ".txt", "w", encoding="utf8")
    all.write(get_words_count_output(words_count))
    all.close()
    
    pos = open("./words_count/" + file_name + "_pos" + ("_balanced" if BALANCED_DATASET else "") + ".txt", "w", encoding="utf8")
    pos.write(get_words_count_output(words_true_count))
    pos.close()
    
    neg = open("./words_count/" + file_name + "_neg" + ("_balanced" if BALANCED_DATASET else "") + ".txt", "w", encoding="utf8")
    neg.write(get_words_count_output(words_fake_count))
    neg.close()

###################
###########################################################
# Main code block

# apply text cleaning
logging.info("reading file: tweets.csv")
logging.info("creating dataframe ...")
df             = pd.read_csv("tweets.csv", encoding="utf8")  # open file
df.columns     = ['id', 'keyword', 'location', 'text', 'target']#vytvoření dataframu
df             = df.drop('id', axis=1)#drop id sloupce

# CREATE A BALANCED DATASET 
if BALANCED_DATASET:
    true_balanced = df[df["target"] == 1].head(BALANCED_SIZE)
    fake_balanced = df[df["target"] == 0].head(BALANCED_SIZE)
    df = pd.concat([true_balanced, fake_balanced])

df['text_len'] = df['text'].apply(lambda x: len(x.split(' ')))#přidání délek textů (kolik mají slov)

###################
balance_counts = df.groupby('target')['target'].agg('count').values  # vyvazenost datasetu
# GRAF pred preprocessingem textu
if SHOW_GRAPH_DATASET_BALANCE:
    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=['real disaster'],
        y=[balance_counts[1]],
        name='real disaster',
        text=[balance_counts[1]],
        textposition='auto',
        marker_color="#0400ff"
    ))
    fig.add_trace(go.Bar(
        x=['not real'],
        y=[balance_counts[0]],
        name='not real disasters',
        text=[balance_counts[0]],
        textposition='auto',
        marker_color="#737373"
    ))
    fig.update_layout(
        title='<span style="font-size:32px; font-family:Times New Roman">Dataset distribution by target</span>'
    )
    fig.show() # samotny vypis - otevre to html stranku s grafem

###########################################################
# graf toho jak jsou zpravy dlouhe pro real disasters a not real disasters
if SHOW_GRAPH_TEXT_LEN:
    real        = df[df['target'] == 1]['text_len'].value_counts().sort_index()
    logging.info("\n\n" + str(real.index))
    not_real    = df[df['target'] == 0]['text_len'].value_counts().sort_index()

    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=real.index,
        y=real.values,
        name='real disasters',
        fill='tozeroy',
        marker_color="#0400ff",
    ))
    fig.add_trace(go.Scatter(
        x=not_real.index,
        y=not_real.values,
        name='not real disasters',
        fill='tozeroy',
        marker_color="#737373",
    ))
    fig.update_layout(
        title='<span style="font-size:32px; font-family:Times New Roman">Text lengths by target</span>',
        xaxis_title="N. of tweets",
        yaxis_title="Text length",
    )
    fig.update_xaxes(range=[0, 40])
    fig.show()


logging.info("cleaning text ...")

df             = df.dropna(how="any", axis=0) # odstraneni chybejicich hodnot ("any"=pokud se najde nejaka NA hodnota v radku, "all"=pokud jsou vsechny hodnoty v radku NA, 0=dropne cely radek, 1=dropne sloupec)
get_words_counts(df, "words counts without any text editing", "no_edit")
df['text']     = df['text'].apply(clean_text)#očištění textu
get_words_counts(df, "words counts after text cleaning", "clean_text")

df['text']     = df['text'].apply(remove_stopwords)#odstranění stopwords
get_words_counts(df, "words counts after stopwords removal", "remove_stopwords")
df['text']     = df['text'].apply(stemm_text)#stemming
get_words_counts(df, "words counts after stemming", "stemm_text")
df['text']     = df['text'].apply(lemmatize_text)#lemmatization
get_words_counts(df, "words counts after lemmatization", "lemmatize_text")
df['text']     = df['text'].apply(text_encode)#text encoding fix - removes errors
#print(df.head(20))

balance_counts = df.groupby('target')['target'].agg('count').values  # vyvazenost datasetu
logging.info("dataset balance: " + str(balance_counts)) # [9256 2114] 9256 real disasters x 2114 not real -> imbalanced dataset

###########################################################
# html vypis vyvazenosti datasetu 
if SHOW_GRAPH_DATASET_BALANCE:
    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=['real disaster'],
        y=[balance_counts[1]],
        name='real disaster',
        text=[balance_counts[1]],
        textposition='auto',
        marker_color="#0400ff"
    ))
    fig.add_trace(go.Bar(
        x=['not real'],
        y=[balance_counts[0]],
        name='not real disasters',
        text=[balance_counts[0]],
        textposition='auto',
        marker_color="#737373"
    ))
    fig.update_layout(
        title='<span style="font-size:32px; font-family:Times New Roman">Dataset distribution by target</span>'
    )
    fig.show() # samotny vypis - otevre to html stranku s grafem

###########################################################
# graf toho jak jsou zpravy dlouhe pro real disasters a not real disasters
if SHOW_GRAPH_TEXT_LEN:
    real        = df[df['target'] == 1]['text_len'].value_counts().sort_index()
    not_real    = df[df['target'] == 0]['text_len'].value_counts().sort_index()

    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=real.index,
        y=real.values,
        name='real disasters',
        fill='tozeroy',
        marker_color="#0400ff",
    ))
    fig.add_trace(go.Scatter(
        x=not_real.index,
        y=not_real.values,
        name='not real disasters',
        fill='tozeroy',
        marker_color="#737373",
    ))
    fig.update_layout(
        title='<span style="font-size:32px; font-family:Times New Roman">Text lengths by target</span>',
        xaxis_title="N. of tweets",
        yaxis_title="Text length",
    )
    fig.update_xaxes(range=[0, 40])
    fig.show()

###########################################################
logging.info("splitting positive/negative sentences ...")
true_df = df[df["target"] == 1]
fake_df = df[df["target"] == 0]

sentences     = []
pos_sentences = []
neg_sentences = []

for i, line in df.iterrows():
    sentences.append(df.at[i, "text"])
    if df.at[i, "target"] == 1:
        pos_sentences.append((1, df.at[i, "text"]))
    if df.at[i, "target"] == 0:
        neg_sentences.append((0, df.at[i, "text"]))

"""
ales = "keyword,location,text,target\n"
for i, line in df.iterrows():
    ales += df.at[i, "text"] + "," + str(df.at[i, "target"]) + "\n"
    #ales += df.at[i, "keyword"] +","+ df.at[i, "location"]+","+ df.at[i, "text"] + "," + str(df.at[i, "target"]) + "\n"

csv_ales = open("csv_ales.txt", "w", encoding="utf8")
csv_ales.write(ales)
csv_ales.close()
"""
###################
#logging.info("counting all words ...")
# Create the dictionary with word frequencies for the entire set. words = []

#words, words_count           = get_words_count(sentences)
#logging.info("counting words in positive sentences ...")
#words_true, words_true_count = get_words_count(pos_sentences)
#logging.info("counting words in negative sentences ...")
#words_fake, words_fake_count = get_words_count(neg_sentences)

#logging.info("counting done")
#logging.info("top 10 all words")
#logging.info(words_count[:10])
#logging.info("top 10 words positive sentences")
#logging.info(words_true_count[:10])
#logging.info("top 10 words negative sentences")
#logging.info(words_fake_count[:10])
########################################################### 
# most, least frequent words
#print(word_count_sorted[:10])
#print(word_count_sorted[-10:])

# N-Grams
if DO_NGRAM_COUNTS:
    logging.info("number of sentences: " + str(len(sentences)))
    logging.info("generating 2-grams ...")
    bi_grams    = get_ngram_counts(2, sentences)
    logging.info("generating 3-grams ...")
    tri_grams   = get_ngram_counts(3, sentences)
    logging.info("generating 4-grams ...")
    four_grams  = get_ngram_counts(4, sentences)
    logging.info("ngrams done")

#print("\nbi_grams")
#print_dict(bi_grams, 20)
#print("\ntri_grams")
#print_dict(tri_grams, 20)
#print("\nfour_grams")
#print_dict(four_grams, 20)


###########################################################
def do_plot(classifier, x_train, y_train, x_test, y_test, predicted, report, classifier_name):
    logging.info(classifier_name + " classifier report")
    logging.info("\n" + report)
    logging.info(classifier_name + " classifier confusion matrix" + "\n" + str(confusion_matrix(y_true=y_test, y_pred=predicted)))
    logging.info(classifier_name + " precision: " + str(classifier.score(x_train, y_train)))
    logging.info(classifier_name + " precision: " + str(classifier.score(x_test, y_test)))

    if DO_PLOT_CLASSIFICATIONS:
        my_plot(classifier, x_test, y_test, "Confusion matrix non-normalized, " + classifier_name + " classifier", ["Negative", "Positive"])
        my_plot(classifier, x_test, y_test, "Confusion matrix normalized, " + classifier_name + " classifier", ["Negative", "Positive"], "true")
        plt.show()

###################
vectorizer_tunned = CountVectorizer(stop_words="english", strip_accents='unicode', lowercase=False)#, ngram_range=(1,2), min_df=0.1, max_df=0.7, max_features=100)
matrix     = vectorizer_tunned.fit_transform(df["text"])
x_train, x_test, y_train, y_test = train_test_split(matrix, df["target"], train_size=0.75, test_size=0.25, random_state=89, stratify=df["target"])

logging.info("train size: " + str(x_train.shape))
logging.info("test size: " + str(x_test.shape))
 
# 500: 83,64; 1000: 84,67; 1500: 86,65; 2000: 87,64; 3000: 88,65; 4000: 91,61;
logging.info("count vectorizer init")
###################
# dummy classifier
dummy           = DummyClassifier()
dummy.fit(x_train, y_train)
dummy_predicted = dummy.predict(x_test)
report          = classification_report(y_true=y_test, y_pred=dummy_predicted)
do_plot(dummy, x_train, y_train, x_test, y_test, dummy_predicted, report, "dummy")

# MultinomialNB
mnb             = MultinomialNB()
mnb.fit(x_train, y_train)
mnb_predicted   = mnb.predict(x_test)
report          = classification_report(y_true=y_test, y_pred=mnb_predicted)
do_plot(mnb, x_train, y_train, x_test, y_test, mnb_predicted, report, "MNB")

# LinearSVC
lsvc    = LinearSVC()
lsvc.fit(x_train, y_train)
lsvc_predicted = lsvc.predict(x_test)
report = classification_report(y_true=y_test, y_pred=lsvc_predicted)
do_plot(lsvc, x_train, y_train, x_test, y_test, lsvc_predicted, report, "linearSVC")

# DecisionTreeClassifier
dtc = DecisionTreeClassifier()
dtc.fit(x_train, y_train)
dtc_predicted = dtc.predict(x_test)
report = classification_report(y_true=y_test, y_pred=dtc_predicted)
do_plot(dtc, x_train, y_train, x_test, y_test, dtc_predicted, report, "DecisionTreeClassifier")

"""
# GaussianProcessClassifier
gpc = GaussianProcessClassifier()
gpc.fit(x_train, y_train)
gpc_predicted = gpc.predict(x_test)
logging.info("precision: " + str(gpc.score(x_train, y_train)))
logging.info("precision: " + str(gpc.score(x_test, y_test)))
report = classification_report(y_true=y_test, y_pred=gpc_predicted)
do_plot(gpc, x_test, y_test, gpc_predicted, report, "GaussianProcessClassifier")
"""